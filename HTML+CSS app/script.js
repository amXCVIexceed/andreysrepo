let selectedRow = 1;

function randomColorId1(){
  let colorId1 = document.getElementById("id1");
  colorId1.style.background = getRandomColor();
}

function addRow(){
  const table = document.getElementById("tableTasks");
  const row = table.insertRow(tableTasks.rows.length);
  row.id = `id${tableTasks.rows.length}`;
  const cellOneInRow = row.insertCell(0);
  cellOneInRow.style.padding = "15px 20px";
  cellOneInRow.innerHTML = `<input type='checkbox' unchecked name="checkbox${tableTasks.rows.length}" id='idCheckbox${tableTasks.rows.length}'>`;
  const cellTwoInRow = row.insertCell(1);
  cellOneInRow.innerHTML = '<b>' + document.getElementById("myInput").value + '</b>';
  document.getElementById(`id${tableTasks.rows.length}`).style.background = getRandomColor();
}

function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function getNumRow(event) {
  with(event.target || event.srcElement) {
    selectedRow = parentNode.rowIndex + 1;
  }
}

function getColor(inputColor) {
  let stateCheckbox;
  for (let i = 0; i < document.getElementById("tableTasks").rows.length; i++) {
    stateCheckbox = document.getElementById(`idCheckbox${i + 1}`);
    if (stateCheckbox.checked) {
      document.getElementById(`id${i + 1}`).style.background = inputColor;
    }
  }
  document.getElementById(`id${selectedRow}`).style.background = inputColor;
}

