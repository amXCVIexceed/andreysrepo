// controllers/products.js
const client = require('../db/index')

exports.product_create = async function (req, res) {
    try {
        const query = `INSERT INTO product 
                        (name, price, quantity)
                        VALUES ('${req.body.name}', 
                                ${req.body.price}, 
                                ${req.body.quantity});`;

        if (!req.body.name || !req.body.price || !req.body.quantity) {
            throw new SyntaxError("Error input data");
        }
        await client.query(query);
        res.send('Added successfully!');
    }
    catch (err) {
        res.send(`${err.message}`);
    }
};

exports.product_delete = async function (req, res) {
    try {
        const query = `DELETE FROM product WHERE id = '${req.body.id}';`;
        if (!req.body.id) {
            throw new SyntaxError("uninitialized id");
        }
        const response = await client.query(query)

        if (!response.rowCount) {
            throw new Error("no such id");
        }
        res.send('Deleted successfully!');
    }
    catch (err) {
        if (err.name == 'SyntaxError') {
            res.send(`${err.message}`);
        }
        else {
            res.status(404).send("Not found. No such id.");
        }
    }
};

exports.product_get = async function (req, res) {
    try {
        const query = `SELECT * FROM product WHERE id = '${req.body.id}';`;
        if (!req.body.id) {
            throw new SyntaxError("uninitialized id");
        }
        const response = await client.query(query)
        res.send(`id:${response.rows[0].id}, name:${response.rows[0].name}, price:${response.rows[0].price}`);
    }
    catch (err) {
        if (err.name == 'SyntaxError') {
            res.send(`${err.message}`);
        }
        else {
            res.status(404).send("Not found. No such id.");
        }
    }
};

exports.product_update = async function (req, res) {
    try {
        if (!req.body.id) {
            throw new SyntaxError("uninitialized id");
        }
        const query = `UPDATE product SET 
                        ${req.body.name ? `name = '${req.body.name}', ` : ''}
                        ${req.body.price ? `price = ${req.body.price}, ` : ''}
                        ${req.body.quantity ? `quantity = ${req.body.quantity}, ` : ''} WHERE id = ${req.body.id};`;
        const replaceComma = query.replace(/,  WHERE/g, ' WHERE ');
        await client.query(replaceComma);
        res.send(`Update succesfully!`);
    }
    catch (err) {
        if (err.name == 'SyntaxError') {
            res.send(`${err.message}`);
        }
        else {
            res.status(404).send("Not found. No such id.");
        }
    }
};