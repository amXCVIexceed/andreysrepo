exports.up = pgm => {
    pgm.createTable('product', {
      id: 'id',
      name: { type: 'varchar(100)', notNull: true },
      price: { type: 'real', notNull: false },
      quantity: { type: 'integer' },
    });

    pgm.createTable('user', {
      id: 'id',
      name: { type: 'varchar(100)', notNull: true },
      purchases: { type: 'integer'}
    });
  };