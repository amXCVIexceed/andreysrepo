require('dotenv').config({ path: __dirname+'/../.env' })
const bodyParser = require('body-parser');
const express = require('express')

const product = require('../routes/product.route'); // Imports routes for the products
const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/products', product);

const port = 1234;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});