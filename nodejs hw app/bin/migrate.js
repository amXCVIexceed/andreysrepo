
require('dotenv').config({ path: __dirname + '/../.env' })
const fs = require('fs');


const config = {
    "db": {
        "user": process.env.DB_USER,
        "password": process.env.DB_PASSWORD,
        "host": process.env.DB_HOST,
        "port": 5432,
        "database": process.env.DB_DATABASE
    }
}

fs.writeFile(__dirname + '/../config/default.json', JSON.stringify(config), function (err) {
    if (err) {
        return console.log(err);
    }
});
