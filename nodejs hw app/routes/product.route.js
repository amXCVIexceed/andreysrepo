const express = require('express');
const router = express.Router();

const product_controller = require('../controllers/product.controller');

module.exports = router;

router.post('/create', product_controller.product_create);

router.post('/delete', product_controller.product_delete);

router.get('/get', product_controller.product_get);

router.post('/update', product_controller.product_update);